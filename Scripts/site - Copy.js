﻿$(document).ready(function () {
	console.log("loaded");
	//alert("loaded");

	$(".nav-button").click(function () {
		var el = $(this);
		if (el.hasClass("clicked")) {
			el.removeClass("clicked");
			$("menu").removeClass("menu-open");
		}
		else {
			el.addClass("clicked");
			$("menu").addClass("menu-open");
		}
	});

	var currentSlideShow = 0;
	function switchSlideShow() {
		var slideShow = $(".slider>ul>li");
		currentSlideShow++;
		if (currentSlideShow == slideShow.length) {
			currentSlideShow = 0;
		}
		slideShow.removeClass("im-visible");
		$(slideShow[currentSlideShow]).addClass("im-visible");
		setTimeout(switchSlideShow, 10000);
	}
	setTimeout(switchSlideShow, 10000);

	function onWindowScroll() {
		var scrollTop = $(window).scrollTop();
		$(".slider>ul>li").css("margin-top", scrollTop/1.5);
	}
	//document.addEventListener("scroll", onWindowScroll);
	$(window).on("scroll", onWindowScroll);


	function initFormValidation() {
		$("input,textarea").each(function () {
			var el = $(this);
			if (el.attr("required")) {
				el.attr("data-required", "true");
				el.removeAttr("required");
			}
		});

		$("#ContactForm").on("submit", function () {
			var validated = true; //everything is not validated by default

			var validationMessage = ["Please correct the following before submitting:"];

			$("input,textarea").each(function () {
				var el = $(this);
				if (el.attr("data-required") == "true") {
					if (this.value === "") {
						validationMessage.push(this.name + " is required.");
						validated = false;

						//we need to also validate email
					}
					if (this.name == "Email") {
						if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.value)) {
							validationMessage.push("Please enter a correct email.");
							validated = false;
						}
					}
				}
			});

			if (!validated) {
				alert(validationMessage.join("\r\n"));
			}

			return validated;
		});
	}
	initFormValidation();


	function initPhotoGallery() {
		$("#PhotoGallery ul li").on("click", function () {
			var imageSrc = $(this).find("img").attr("src");
			var lightBox = $(".light-box");
			lightBox.find("img").attr("src", imageSrc);

			lightBox.addClass("im-visible");
		});

		$(".light-box #CloseButton").on("click", function () {
			$(".light-box").removeClass("im-visible");
		});
	}
	initPhotoGallery();

});