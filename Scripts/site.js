﻿$(document).ready(function () {
	$(".nav-button").click(function () {
		var el = $(this);
		if (el.hasClass("clicked")) {
			el.removeClass("clicked");
			$("menu,.wrapper").removeClass("menu-open");
		}
		else {
			el.addClass("clicked");
			$("menu,.wrapper").addClass("menu-open");
		}
	});

	var currentSlideShow = 0;
	function switchSlideShow() {
		var slideShow = $(".slider>ul>li");
		currentSlideShow++;
		if (currentSlideShow == slideShow.length) {
			currentSlideShow = 0;
		}
		slideShow.removeClass("im-visible");
		$(slideShow[currentSlideShow]).addClass("im-visible");
		setTimeout(switchSlideShow, 5000);
	}
	setTimeout(switchSlideShow, 5000);

	function onWindowScroll() {
		var scrollTop = $(window).scrollTop();
		$(".slider>ul>li").css("margin-top", scrollTop / 1.5);
	}
	$(window).on("scroll", onWindowScroll);

	
	function initPhotoGallery() {
		var lightBox = $(".light-box");
		currentGalleryImage = 0;
		galleryLength = $("#PhotoGallery ul li").length;

		$("#PhotoGallery ul li").each(function (i) {
			$(this).attr("data-index", i);
		});
		$("#PhotoGallery ul li").on("click", function () {
			var imageSrc = $(this).find("img").attr("src");
			lightBox.find("img").attr("src", imageSrc);
			lightBox.addClass("im-visible");

			currentGalleryImage = parseInt($(this).attr("data-index"));
		});

		$(".light-box #CloseButton").on("click", function () {
			$(".light-box").removeClass("im-visible");
		});

		$(".prev, .next").on("click", function () {
			var $el = $(this);
			if ($el.hasClass("prev")) {
				currentGalleryImage--;
				if (currentGalleryImage < 0)
					currentGalleryImage = galleryLength - 1;
			}
			if ($el.hasClass("next")) {
				currentGalleryImage++;
				if (currentGalleryImage >= galleryLength)
					currentGalleryImage = 0;
			}
			$("#PhotoGallery ul li:nth-child(" + (currentGalleryImage + 1) + ")").trigger("click");
		});
	}
	initPhotoGallery();


	function initFormValidation() {
		$("input,textarea").each(function () {
			var el = $(this);
			if (el.attr("required")) {
				el.attr("data-required", "true");
				el.removeAttr("required");
			}
		});

		$("#ContactForm").on("submit", function () {
			var validated = true; //everything is not validated by default

			var validationMessage = ["Please correct the following before submitting:"];

			$("input,textarea").each(function () {
				var el = $(this);
				if (el.attr("data-required") == "true") {
					if (this.value === "") {
						validationMessage.push(this.name + " is required.");
						validated = false;

						//we need to also validate email
					}
					if (this.name == "Email") {
						if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.value)) {
							validationMessage.push("Please enter a correct email.");
							validated = false;
						}
					}
				}
			});

			if (!validated) {
				alert(validationMessage.join("\r\n"));
			}

			return validated;
		});
	}
	initFormValidation();
});